﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Windows.ApplicationModel.AppService;
using Windows.Foundation.Collections;

namespace _9_AppService_ClientWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void OnCallingService(object sender, RoutedEventArgs e)
        {
            var connection = new AppServiceConnection
            {
                PackageFamilyName = "7127b7e8-216f-4d33-a7f7-87a5e798dac8_ecet6zh215f6e",
                AppServiceName = "RandomNumberService"
            };
            var connectionStatus = await connection.OpenAsync();
            if (connectionStatus == AppServiceConnectionStatus.Success)
            {
                var message = new ValueSet
                {
                    {"Command", "GetInt"},
                    {"MinValue", Convert.ToInt32(MinValue.Text)},
                    {"MaxValue", Convert.ToInt32(MaxValue.Text)}
                };

                var response = await connection.SendMessageAsync(message);
                if (response.Status == AppServiceResponseStatus.Success)
                {
                    Result.Text = ((int)response.Message["Result"]).ToString();
                }
            }
        }
    }
}
