﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace _4_ProtocolHandler
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private ProtocolForResultsOperation _operation;

        public MainPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter is ProtocolActivatedEventArgs)
            {
                var protocolArgs = (ProtocolActivatedEventArgs) e.Parameter;
                Uri.Text = protocolArgs.Uri.ToString();

                if ((protocolArgs.Data != null) && protocolArgs.Data.ContainsKey("File"))
                {
                    var fileToken = (string) protocolArgs.Data["File"];
                    LoadImageAsync(fileToken);
                }
            }

            if (e.Parameter is ProtocolForResultsActivatedEventArgs)
            {
                var protocolArgs = (ProtocolForResultsActivatedEventArgs) e.Parameter;
                Uri.Text = protocolArgs.Uri.ToString();
                _operation = protocolArgs.ProtocolForResultsOperation;
                ResultPanel.Visibility = Visibility.Visible;
            }
        }

        private async void LoadImageAsync(string token)
        {
            var file = await SharedStorageAccessManager.RedeemTokenForFileAsync(token);
            var image = new BitmapImage();
            await image.SetSourceAsync(await file.OpenAsync(FileAccessMode.Read));
            InputImage.Source = image;
        }

        private void OnReturningResult(object sender, RoutedEventArgs e)
        {
            if (_operation != null)
            {
                var result = new ValueSet
                {
                    {"Result", Result.Text}
                };
                _operation.ReportCompleted(result);
            }
        }
    }
}
