﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.DataTransfer;
using Windows.ApplicationModel.DataTransfer.ShareTarget;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace _2_SharingTarget
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private ShareOperation _shareOperation;

        public MainPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter is ShareOperation)
            {
                DisplaySharedText((ShareOperation)e.Parameter);
            }
        }

        private async void DisplaySharedText(ShareOperation shareOperation)
        {
            _shareOperation = shareOperation;
            SharedContent.Text = await shareOperation.Data.GetTextAsync();
            if (!string.IsNullOrEmpty(shareOperation.QuickLinkId))
            {
                QuickLinkId.Text = shareOperation.QuickLinkId;
            }
        }

        private void OnCreatingQuickLink(object sender, RoutedEventArgs e)
        {
            if (_shareOperation != null && !string.IsNullOrEmpty(QuickLinkId.Text))
            {
                var quickLink = new QuickLink
                {
                    Id = QuickLinkId.Text,
                    SupportedFileTypes = { },
                    SupportedDataFormats = { StandardDataFormats.Text },
                    Thumbnail = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/Square44x44Logo.scale-200.png")),
                    Title = $"{QuickLinkId.Text} QuickLink"
                };
                _shareOperation.ReportCompleted(quickLink);
            }
        }
    }
}
