﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.AppService;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace _9_AppService_ClientUWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void OnCallingService(object sender, RoutedEventArgs e)
        {
            var connection = new AppServiceConnection
            {
                PackageFamilyName = "7127b7e8-216f-4d33-a7f7-87a5e798dac8_ecet6zh215f6e",
                AppServiceName = "RandomNumberService"
            };
            var connectionStatus = await connection.OpenAsync();
            if (connectionStatus == AppServiceConnectionStatus.Success)
            {
                var message = new ValueSet
                {
                    {"Command", "GetInt"},
                    {"MinValue", Convert.ToInt32(MinValue.Text)},
                    {"MaxValue", Convert.ToInt32(MaxValue.Text)}
                };

                var response = await connection.SendMessageAsync(message);
                if (response.Status == AppServiceResponseStatus.Success)
                {
                    Result.Text = ((int)response.Message["Result"]).ToString();
                }
            }
        }
    }
}
