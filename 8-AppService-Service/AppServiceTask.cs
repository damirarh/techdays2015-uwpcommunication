﻿using System;
using Windows.ApplicationModel.AppService;
using Windows.ApplicationModel.Background;
using Windows.Foundation.Collections;

namespace _8_AppService_Service
{
    public sealed class AppServiceTask : IBackgroundTask
    {
        BackgroundTaskDeferral _serviceDeferral;
        Random _random;

        public void Run(IBackgroundTaskInstance taskInstance)
        {
            _serviceDeferral = taskInstance.GetDeferral();

            taskInstance.Canceled += OnTaskCanceled;

            _random = new Random();

            var trigger = taskInstance.TriggerDetails as AppServiceTriggerDetails;
            if (trigger?.Name == "RandomNumberService")
            {
                trigger.AppServiceConnection.RequestReceived += OnRequestReceived;
            }
        }

        private void OnTaskCanceled(IBackgroundTaskInstance sender, BackgroundTaskCancellationReason reason)
        {
            if (_serviceDeferral != null)
            {
                _serviceDeferral.Complete();
                _serviceDeferral = null;
            }
        }

        private async void OnRequestReceived(AppServiceConnection sender, AppServiceRequestReceivedEventArgs args)
        {
            var messageDeferral = args.GetDeferral();

            try
            {
                var input = args.Request.Message;

                var command = (string) input["Command"];
                switch (command)
                {
                    case "GetInt":
                        var minValue = (int)input["MinValue"];
                        var maxValue = (int)input["MaxValue"];

                        var result = new ValueSet
                        {
                            {"Result", _random.Next(minValue, maxValue)}
                        };
                        await args.Request.SendResponseAsync(result);
                        break;
                }
            }
            finally
            {
                messageDeferral.Complete();
            }
        }
    }
}
