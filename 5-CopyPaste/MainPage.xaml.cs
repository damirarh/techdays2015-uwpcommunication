﻿using System;
using Windows.ApplicationModel.DataTransfer;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace _5_CopyPaste
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void OnCopying(object sender, RoutedEventArgs e)
        {
            var dataPackage = new DataPackage
            {
                RequestedOperation = DataPackageOperation.Copy,
            };
            dataPackage.SetText($"{Ip1.Text}.{Ip2.Text}.{Ip3.Text}.{Ip4.Text}");
            Clipboard.SetContent(dataPackage);
        }

        private async void OnPasting(object sender, RoutedEventArgs e)
        {
            var dataPackageView = Clipboard.GetContent();
            if ((dataPackageView != null) && dataPackageView.Contains(StandardDataFormats.Text))
            {
                var components = (await dataPackageView.GetTextAsync()).Split('.');
                Array.Resize(ref components, 4);
                Ip1.Text = components[0];
                Ip2.Text = components[1];
                Ip3.Text = components[2];
                Ip4.Text = components[3];
            }
        }

        private void OnTrackClipboardChecked(object sender, RoutedEventArgs e)
        {
            Clipboard.ContentChanged += OnClipboardContentChanged;
        }

        private void OnTrackClipboardUnchecked(object sender, RoutedEventArgs e)
        {
            Clipboard.ContentChanged -= OnClipboardContentChanged;
        }

        private async void OnClipboardContentChanged(object sender, object e)
        {
            var dataPackageView = Clipboard.GetContent();
            if ((dataPackageView != null) && dataPackageView.Contains(StandardDataFormats.Text))
            {
                LastClipboardContent.Text = await dataPackageView.GetTextAsync();
            }
        }
    }
}
