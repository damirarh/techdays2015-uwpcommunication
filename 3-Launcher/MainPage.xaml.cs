﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace _3_Launcher
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private readonly Uri _uri = new Uri("mailto:damir.arh@gmail.com");
        private readonly string _packageFamilyName = "b2f0053d-6c2a-4503-b28f-57093f0bfad0_ecet6zh215f6e";

        private LauncherOptions GetOptions()
        {
            return new LauncherOptions
            {
                TargetApplicationPackageFamilyName = _packageFamilyName
            };
        }

        private async Task<string> PickFileAsync()
        {
            FileOpenPicker picker = new FileOpenPicker
            {
                ViewMode = PickerViewMode.Thumbnail,
                SuggestedStartLocation = PickerLocationId.PicturesLibrary,
                FileTypeFilter = { ".jpg", ".jpeg", ".png" }
            };
            var file = await picker.PickSingleFileAsync();

            return file != null ? SharedStorageAccessManager.AddFile(file) : null;
        }

        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void OnLaunchingDefault(object sender, RoutedEventArgs e)
        {
            await Launcher.LaunchUriAsync(_uri);
        }

        private async void OnLaunchingSpecific(object sender, RoutedEventArgs e)
        {
            await Launcher.LaunchUriAsync(_uri, GetOptions());
        }

        private async void OnSendingFile(object sender, RoutedEventArgs e)
        {
            var token = await PickFileAsync();

            if (token != null)
            {
                var input = new ValueSet
                {
                    {"File", token}
                };
                await Launcher.LaunchUriAsync(_uri, GetOptions(), input);
            }
        }

        private async void OnLaunchingForResults(object sender, RoutedEventArgs e)
        {
            var result = await Launcher.LaunchUriForResultsAsync(_uri, GetOptions());
            if (result.Status == LaunchUriStatus.Success)
            {
                if ((result.Result != null) && result.Result.ContainsKey("Result"))
                {
                    if (result.Result["Result"] != null)
                    {
                        Result.Text = (string) result.Result["Result"];
                    }
                }
            }
        }

        private async void OnCheckingIfInstalled(object sender, RoutedEventArgs e)
        {
            var result = await Launcher.QueryUriSupportAsync(_uri, LaunchQuerySupportType.UriForResults, _packageFamilyName);
            Status.Text = result.ToString();
        }
    }
}
